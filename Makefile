.POSIX:

include config.mk

SRC = diction.c
OBJ = $(SRC:.c=.o)

all: options diction

options:
	@echo dictionary build options:
	@echo "CFLAGS	= $(STCFLAGS)"
	@echo "LDFLAGS	= $(STLDFLAGS)"
	@echo "CC	= $(CC)"

.c.o:
	$(CC) $(STCFLAGS) -c $<

$(OBJ): config.h config.mk

diction: $(OBJ)
	$(CC) -o $@ $(OBJ) $(STLDFLAGS)

clean:
	rm -f diction $(OBJ) diction-$(VERSION).tar.gz

install: diction
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f diction $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/diction

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/diction


.PHONY: all options clean dist install uninstall


