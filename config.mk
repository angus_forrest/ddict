VERSION = 0.1.0

PREFIX = /usr/local
MANPREFIX = $(PREFIX)/share/man

LIBS = -lcjson

STCPPFLAGS = -DVERSION=\"$(VERSION)\"
STCFLAGS = 
STLDFLAGS = $(LIBS) $(LDFLAGS)
