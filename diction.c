#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include <cjson/cJSON.h>
#include "config.h"

#define MAXPATH 1024

char *
getBuffer(char * string)
{
	FILE *fp = fopen(string, "rb");
	fseek(fp, 0, SEEK_END);
	long fsize = ftell (fp);
	fseek(fp, 0, SEEK_SET);
	char *buffer = malloc(fsize +1);
	fread(buffer, 1, fsize, fp);
	fclose(fp);
	return buffer;
}

char *
setPrefix()
{
	char *prefix = malloc(MAXPATH*sizeof(char));
	strncpy(prefix, getenv(programFolderPath), MAXPATH);
	strcat(prefix, "/");
	strcat(prefix, programFolder);
	strcat(prefix, "/");
	return prefix;
}
char *
getFilePath(char *prefix, char *file)
{
	char *filePath = malloc(MAXPATH*sizeof(char));
	strcpy(filePath, prefix);
	strcat(filePath, "/");
	strcat(filePath, file);
	return filePath;
}

int
main()
{
	char *prefix = setPrefix();
	char *filePath = getFilePath(prefix, "test2.json");
	char *buffer = getBuffer(filePath);

	cJSON *json_file = cJSON_Parse(buffer);
	
	if(json_file == NULL)
	{
		const char *error_ptr = cJSON_GetErrorPtr();
		if(error_ptr != NULL)
		{
			fprintf(stderr, "Error before: %s\n", error_ptr);
		}
		exit(1);
	}
	cJSON *json_dictions = cJSON_GetObjectItem(json_file, "dictions");
	cJSON *json_diction = NULL;

	//cJSON pointer's first level
	cJSON **json_language, **json_region, **json_words, **json_definitions;
	json_language = malloc(sizeof(cJSON *)*cJSON_GetArraySize(json_dictions));
	json_region = malloc(sizeof(cJSON *)*cJSON_GetArraySize(json_dictions));
	json_words =  malloc(sizeof(cJSON *)*cJSON_GetArraySize(json_dictions));
	json_definitions = malloc(sizeof(cJSON *)*cJSON_GetArraySize(json_dictions));
	//cJSON pointer's second level
	cJSON ***json_wordLanguage, ***json_spelling, ***json_pronounications, ***json_denote, ***json_type, ***json_connotes, ***json_synonyms, ***json_antonyms, ***json_examples;
	json_wordLanguage = malloc(sizeof(cJSON **)*cJSON_GetArraySize(json_dictions));
	json_spelling = malloc(sizeof(cJSON **)*cJSON_GetArraySize(json_dictions));
	json_pronounications = malloc(sizeof(cJSON **)*cJSON_GetArraySize(json_dictions));
	json_denote = malloc(sizeof(cJSON **)*cJSON_GetArraySize(json_dictions));
	json_type = malloc(sizeof(cJSON **)*cJSON_GetArraySize(json_dictions));
	json_connotes = malloc(sizeof(cJSON **)*cJSON_GetArraySize(json_dictions));
	json_synonyms = malloc(sizeof(cJSON **)*cJSON_GetArraySize(json_dictions));
	json_antonyms = malloc(sizeof(cJSON **)*cJSON_GetArraySize(json_dictions));
	json_examples = malloc(sizeof(cJSON **)*cJSON_GetArraySize(json_dictions));
	//cJSON pointer's third level
	cJSON ****json_ipa, ****json_ipaRegion, ****json_sentence, ****json_context;
	json_ipa = malloc(sizeof(cJSON ***)*cJSON_GetArraySize(json_dictions));
	json_ipaRegion = malloc(sizeof(cJSON ***)*cJSON_GetArraySize(json_dictions));
	json_sentence = malloc(sizeof(cJSON ***)*cJSON_GetArraySize(json_dictions));
	json_context = malloc(sizeof(cJSON ***)*cJSON_GetArraySize(json_dictions));
	
	int ITER_ONE =0;
	cJSON_ArrayForEach(json_diction, json_dictions)
	{
		json_language[ITER_ONE] = cJSON_GetObjectItemCaseSensitive(json_diction, "language");
		json_region[ITER_ONE] = cJSON_GetObjectItemCaseSensitive(json_diction, "region");
		json_words[ITER_ONE] = cJSON_GetObjectItemCaseSensitive(json_diction, "words");
		json_definitions[ITER_ONE] = cJSON_GetObjectItemCaseSensitive(json_diction, "definitions");
		ITER_ONE++;
	}

	for(int i = 0; i<cJSON_GetArraySize(json_dictions);i++)
	{
		json_wordLanguage[i] = malloc(sizeof(cJSON *)*cJSON_GetArraySize(json_words[i]));
		json_spelling[i] = malloc(sizeof(cJSON *)*cJSON_GetArraySize(json_words[i]));
		json_pronounications[i] = malloc(sizeof(cJSON *)*cJSON_GetArraySize(json_words[i]));
		json_ipa[i] = malloc(sizeof(cJSON **)*cJSON_GetArraySize(json_words[i]));
		json_ipaRegion[i] = malloc(sizeof(cJSON **)*cJSON_GetArraySize(json_words[i]));
	}
	for(int i = 0; i<cJSON_GetArraySize(json_dictions);i++)
	{
		json_denote[i] = malloc(sizeof(cJSON *)*cJSON_GetArraySize(json_definitions[i]));
		json_type[i] = malloc(sizeof(cJSON *)*cJSON_GetArraySize(json_definitions[i]));
		json_connotes[i] = malloc(sizeof(cJSON *)*cJSON_GetArraySize(json_definitions[i]));
		json_synonyms[i] = malloc(sizeof(cJSON *)*cJSON_GetArraySize(json_definitions[i]));
		json_antonyms[i] = malloc(sizeof(cJSON *)*cJSON_GetArraySize(json_definitions[i]));
		json_examples[i] = malloc(sizeof(cJSON *)*cJSON_GetArraySize(json_definitions[i]));
		json_sentence[i] = malloc(sizeof(cJSON **)*cJSON_GetArraySize(json_definitions[i]));
		json_context[i] = malloc(sizeof(cJSON **)*cJSON_GetArraySize(json_definitions[i]));
	}

	//Layer 2
	printf("Layer 2\n");
	ITER_ONE =0;
	cJSON_ArrayForEach(json_diction, json_dictions)
	{
		int ITER_TWO = 0;
		cJSON *json_word = NULL;
		cJSON_ArrayForEach(json_word, json_words[ITER_ONE])
		{
			json_wordLanguage[ITER_ONE][ITER_TWO] = cJSON_GetObjectItemCaseSensitive(json_word, "wordLanguage");
			json_spelling[ITER_ONE][ITER_TWO] = cJSON_GetObjectItemCaseSensitive(json_word, "spelling");
			json_pronounications[ITER_ONE][ITER_TWO] = cJSON_GetObjectItemCaseSensitive(json_word, "pronounications");

			ITER_TWO++;
		}
		ITER_TWO = 0;
		cJSON *json_definition = NULL;
		cJSON_ArrayForEach(json_definition, json_definitions[ITER_ONE])
		{
			json_denote[ITER_ONE][ITER_TWO] = cJSON_GetObjectItemCaseSensitive(json_definition, "denote");
			json_type[ITER_ONE][ITER_TWO] = cJSON_GetObjectItemCaseSensitive(json_definition, "type");
			json_connotes[ITER_ONE][ITER_TWO] = cJSON_GetObjectItemCaseSensitive(json_definition, "connnotes");
			json_synonyms[ITER_ONE][ITER_TWO] = cJSON_GetObjectItemCaseSensitive(json_definition, "synonyms");
			json_antonyms[ITER_ONE][ITER_TWO] = cJSON_GetObjectItemCaseSensitive(json_definition, "antonyms");
			json_examples[ITER_ONE][ITER_TWO] = cJSON_GetObjectItemCaseSensitive(json_definition, "examples");
			ITER_TWO++;
		}
		ITER_ONE++;
	}
	printf("allocate\n");

	for(int i = 0; i<cJSON_GetArraySize(json_dictions);i++)
	{
		for(int j = 0; j < cJSON_GetArraySize(json_words[i]);j++)
		{
			json_ipa[i][j] = malloc(sizeof(cJSON *)*cJSON_GetArraySize(json_pronounications[i][j]));
			json_ipaRegion[i][j] = malloc(sizeof(cJSON *)*cJSON_GetArraySize(json_pronounications[i][j]));
		}
	}
	for(int i = 0; i<cJSON_GetArraySize(json_dictions);i++)
	{
		for(int j = 0; j < cJSON_GetArraySize(json_definitions[i]);j++)
		{
			json_sentence[i][j] = malloc(sizeof(cJSON *)*cJSON_GetArraySize(json_examples[i][j]));
			json_context[i][j] = malloc(sizeof(cJSON *)*cJSON_GetArraySize(json_examples[i][j]));
		}
	}

	printf("Layer 3\n");

	ITER_ONE =0;
	cJSON_ArrayForEach(json_diction, json_dictions)
	{
		int ITER_TWO = 0;
		cJSON *json_word = NULL;
		cJSON_ArrayForEach(json_word, json_words[ITER_ONE])
		{
			int ITER_THR = 0;
			cJSON *json_pronounication = NULL;
			cJSON_ArrayForEach(json_pronounication, json_pronounications[ITER_ONE][ITER_TWO])
			{
				json_ipa[ITER_ONE][ITER_TWO][ITER_THR] = cJSON_GetObjectItemCaseSensitive(json_pronounication, "ipa");
				json_ipaRegion[ITER_ONE][ITER_TWO][ITER_THR] = cJSON_GetObjectItemCaseSensitive(json_pronounication, "ipaRegion");
				ITER_THR++;
			}
			ITER_TWO++;
		}
		ITER_TWO = 0;
		cJSON *json_definition = NULL;
		cJSON_ArrayForEach(json_definition, json_definitions[ITER_ONE])
		{
			int ITER_THR = 0;
			cJSON *json_example = NULL;
			cJSON_ArrayForEach(json_example, json_examples[ITER_ONE][ITER_TWO])
			{
				json_sentence[ITER_ONE][ITER_TWO][ITER_THR] = cJSON_GetObjectItemCaseSensitive(json_example, "sentence");
				json_context[ITER_ONE][ITER_TWO][ITER_THR] = cJSON_GetObjectItemCaseSensitive(json_example, "context");
				ITER_THR++;
			}
			ITER_TWO++;
		}
		ITER_ONE++;
	}

	printf("%s\n",json_ipa[0][0][0]->valuestring);
	return 0;
}
